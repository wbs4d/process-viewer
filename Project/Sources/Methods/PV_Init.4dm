//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: PV_Init

// Initializes both the process and interprocess variables used by the RE routines.

// Access: Private

// Created by Wayne Stewart (Feb 17, 2006)
//     waynestewart@mac.com
// ----------------------------------------------------

C_BOOLEAN:C305($FoundationPresent_b)

If (Storage:C1525.PV.Initialised=Null:C1517)
	
	ARRAY TEXT:C222($InstalledComponents_at; 0)
	COMPONENT LIST:C1001($InstalledComponents_at)
	$FoundationPresent_b:=(Find in array:C230($InstalledComponents_at; "Foundation@")>0)
	
	Use (Storage:C1525)
		Storage:C1525.PV:=New shared object:C1526(\
			"Initialised"; True:C214; \
			"FoundationPresent"; $FoundationPresent_b)
	End use 
	
End if 



