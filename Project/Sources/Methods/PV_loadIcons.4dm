//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: PV_loadIcons

// Loads the icons

// Access: Private

// Parameters: 
//   $1 : Object : The object to load with icons

// Created by Wayne Stewart (2023-03-01T13:00:00Z)

//     waynestewart@mac.com
// ----------------------------------------------------

#DECLARE($form : Object)

var $path_t; $FullPath_t; $IconName_t; $key : Text
var $i : Integer
var $pic; $blank : Picture

$path_t:=Get 4D folder:C485(Current resources folder:K5:16)+"Images"+Folder separator:K24:12

For ($i; 0; 7)
	$pic:=$blank
	$IconName_t:="PS_54"+String:C10($i)+".png"
	$FullPath_t:=$path_t+$IconName_t
	If (Test path name:C476($FullPath_t)=Is a document:K24:1)
		READ PICTURE FILE:C678($FullPath_t; $pic)
		$key:="stateIcon_"+String:C10($i)
		$form[$key]:=$pic
	End if 
End for 



