//%attributes = {"invisible":true}

// ----------------------------------------------------
// Project Method: Compiler_RE

// Compiler variables related to the RE routines.

// Method Type: Private

// Parameters: None

// Returns: Nothing

// Created by Wayne Stewart (Feb 17, 2006)
//     waynestewart@mac.com
// ----------------------------------------------------

// Interprocess Variables


//C_BOOLEAN(<>PV_Initialized_b)
//If (Not(<>PV_Initialized_b))  // So we only do this once.
//Compiler_PV_Arrays

//`ARRAY PICTURE(<>PV_StateIcons_apic; 0)



//End if 

// Process Variables


// Parameters
If (False:C215)
	C_TEXT:C284(PV_Info; $1; $0)
	C_LONGINT:C283(PV_Launch; $1)
	
	C_OBJECT:C1216(PV_loadIcons; $1)
	
	C_TEXT:C284(UTIL_DateAndTimeToISO; $0)
	C_DATE:C307(UTIL_DateAndTimeToISO; $1)
	C_TIME:C306(UTIL_DateAndTimeToISO; $2)
	
	C_TEXT:C284(UTIL_WriteMethodComments; $1)
	C_BOOLEAN:C305(UTIL_WriteMethodComments; $2; $3)
	
	C_TEXT:C284(UTIL_BuildComponent; $1)
	
End if 
