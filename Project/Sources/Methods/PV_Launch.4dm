//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: PV_Launch

// Launches the Process Viewer dialog

// Access: Shared

// Created by Wayne Stewart (Jun 29, 2013)

//     waynestewart@mac.com
// ----------------------------------------------------


C_LONGINT:C283($1; $ProcessID_i)

var $form_o : Object

If (Count parameters:C259=1)
	PV_Init
	If (Storage:C1525.PV.FoundationPresent)
		EXECUTE METHOD:C1007("Fnd_Wnd_Title"; *; "Process Viewer")
		EXECUTE METHOD:C1007("Fnd_Wnd_Type"; *; (0-(Palette window:K34:3+Has zoom box:K34:9+Has grow box:K34:10+Has window title:K34:11)))
		EXECUTE METHOD:C1007("Fnd_Wnd_CloseBox"; *; True:C214)
		EXECUTE METHOD:C1007("Fnd_Wnd_UseSavedPosition"; *; "Process Viewer")
		EXECUTE METHOD:C1007("Fnd_Wnd_OpenWindow"; *; 551; 292)
		//Fnd_Wnd_OpenWindow (551;257)
	Else 
		$ProcessID_i:=Open form window:C675("Process Viewer"; Palette form window:K39:9; On the right:K39:3; At the bottom:K39:6; *)  //(0-(Palette form window+Has zoom box+Has grow box+Has window title)))
		SET WINDOW TITLE:C213("Process Viewer")
	End if 
	
	
	$form_o:=New object:C1471()
	PV_loadIcons($form_o)
	
	
	
	DIALOG:C40("Process Viewer"; $form_o)
	If (Storage:C1525.PV.FoundationPresent)
		EXECUTE METHOD:C1007("Fnd_Wnd_SavePosition"; *; "Process Viewer")
	End if 
	
	CLOSE WINDOW:C154
	
Else 
	// This version allows for any number of processes
	// $ProcessID_i:=New Process(Current method name;128*1024;Current method name;0)
	// This version allows for one unique process
	$ProcessID_i:=New process:C317(Current method name:C684; 128*1024; "$Process Viewer"; 0; *)
	RESUME PROCESS:C320($ProcessID_i)
	SHOW PROCESS:C325($ProcessID_i)
	BRING TO FRONT:C326($ProcessID_i)
End if 