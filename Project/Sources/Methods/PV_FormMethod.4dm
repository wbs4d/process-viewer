//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: PV_FormMethod

// Form method for dialog

// Access: Private

// Created by Wayne Stewart (Oct 11, 2006)

//     waynestewart@mac.com
// ----------------------------------------------------
If (False:C215)
	PV_FormMethod
End if 


C_LONGINT:C283($l; $t; $r; $b; $h; $Width_i; $ColumnWidth_i; $TimeColumnWidth_i)

If (Form event code:C388=On Load:K2:1)
	PV_LoadArrays
	SET TIMER:C645(60)
	
	//OBJECT SET FONT(*; "@"; "Lucida Grande")
	//OBJECT SET FONT SIZE(*; "@"; 12)
	//OBJECT SET FONT STYLE(*; "@"; Plain)
	
	//OBJECT SET FONT SIZE(*; "Hdr_@"; 11)
	
	OBJECT SET VISIBLE:C603(*; "@"; True:C214)
	
	GET WINDOW RECT:C443($l; $t; $r; $b)
	$Width_i:=$r-$l-20  //  Border of 10 on each size
	$Width_i:=$Width_i-15  //  Scrollbar
	
	LISTBOX SET COLUMN WIDTH:C833(*; "PV_Icon_apic"; 19)
	$Width_i:=$Width_i-20  // Get rid of width of picture column
	LISTBOX SET COLUMN WIDTH:C833(*; "PV_ProcID_ai"; 40)
	$Width_i:=$Width_i-41
	$Width_i:=$Width_i-61  //  Get rid of time
	$ColumnWidth_i:=Int:C8($Width_i/2)
	
	LISTBOX SET COLUMN WIDTH:C833(*; "PV_ProcName_at"; $ColumnWidth_i)
	LISTBOX SET COLUMN WIDTH:C833(*; "PV_ProcState_at"; $ColumnWidth_i)
	
End if 

If (Form event code:C388=On Timer:K2:25)
	PV_LoadArrays
End if 


If (Form event code:C388=On Resize:K2:27)
	GET WINDOW RECT:C443($l; $t; $r; $b)
	$h:=$b-$t-10
	$Width_i:=$r-$l-10
	OBJECT MOVE:C664(*; "PV_ProcList_ab"; 10; 40; $Width_i; $h; *)
	
	
	$Width_i:=$Width_i-15  //  Scrollbar
	$TimeColumnWidth_i:=$Width_i
	
	LISTBOX SET COLUMN WIDTH:C833(*; "PV_Icon_apic"; 19)
	$Width_i:=$Width_i-20  // Get rid of width of picture column
	LISTBOX SET COLUMN WIDTH:C833(*; "PV_ProcID_ai"; 40)
	$Width_i:=$Width_i-41
	$Width_i:=$Width_i-61  //  Get rid of time
	$ColumnWidth_i:=Int:C8($Width_i/2)
	
	LISTBOX SET COLUMN WIDTH:C833(*; "PV_ProcName_at"; $ColumnWidth_i)
	LISTBOX SET COLUMN WIDTH:C833(*; "PV_ProcState_at"; $ColumnWidth_i)
	
	$TimeColumnWidth_i:=-15-19-40-($ColumnWidth_i*2)-15
	LISTBOX SET COLUMN WIDTH:C833(*; "PV_ProcTime_ai"; $TimeColumnWidth_i)
	
	
	OBJECT SET VISIBLE:C603(*; "@"; True:C214)
	
End if 


If (Form event code:C388=On Close Box:K2:21)
	If (Storage:C1525.PV.FoundationPresent)
		// ignore this it will be handled by Fnd_Gen_FormMethod
	Else 
		CANCEL:C270
	End if 
End if 
