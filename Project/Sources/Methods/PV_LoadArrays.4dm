//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: PV_LoadArrays

// Loads the arrays displayed in the dialog

// Access: Private

// Created by Wayne Stewart (2023-03-01T13:00:00Z)

//     waynestewart@mac.com
// ----------------------------------------------------




var $icon_ptr; $ProcID_ptr; $ProcName_ptr; $ProcState_ptr; $ProcTime_ptr; $listbox_ptr : Pointer

C_LONGINT:C283($NumberOfTasks_i; $CurrentProcess_i; $procState; $procTime)
C_TEXT:C284($procName)
C_PICTURE:C286($Blank_pic)

$NumberOfTasks_i:=Count tasks:C335

$icon_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "PV_Icon_apic")
$ProcID_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "PV_ProcID_ai")
$ProcName_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "PV_ProcName_at")
$ProcState_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "PV_ProcState_at")
$ProcTime_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "PV_ProcTime_ai")
$listbox_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "PV_ProcList_ab")



ARRAY BOOLEAN:C223($listbox_ptr->; $NumberOfTasks_i)

ARRAY PICTURE:C279($icon_ptr->; $NumberOfTasks_i)
ARRAY LONGINT:C221($ProcID_ptr->; $NumberOfTasks_i)
ARRAY TEXT:C222($ProcName_ptr->; $NumberOfTasks_i)
ARRAY TEXT:C222($ProcState_ptr->; $NumberOfTasks_i)
ARRAY LONGINT:C221($ProcTime_ptr->; $NumberOfTasks_i)

//Value Description
//0x00000000 Black
//0x00FF0000 Bright Red
//0x0000FF00 Bright Green
//0x000000FF Bright Blue
//0x007F7F7F Gray
//0x00FFFF00 Bright Yellow
//0x00FF7F7F Red Pastel
//0x00FFFFFF White



OBJECT SET RGB COLORS:C628(*; "PV_ProcList_ab"; Foreground color:K23:1; Background color:K23:2; "#EDF3FE")

For ($CurrentProcess_i; 1; $NumberOfTasks_i)
	
	PROCESS PROPERTIES:C336($CurrentProcess_i; $procName; $procState; $procTime)
	
	$ProcID_ptr->{$CurrentProcess_i}:=$CurrentProcess_i
	$ProcName_ptr->{$CurrentProcess_i}:=$procName
	LISTBOX SET ROW FONT STYLE:C1268(*; "PV_ProcList_ab"; $CurrentProcess_i; Plain:K14:1)
	LISTBOX SET ROW COLOR:C1270(*; "PV_ProcList_ab"; $CurrentProcess_i; "black")
	$icon_ptr->{$CurrentProcess_i}:=$Blank_pic
	
	Case of 
			
		: ($procState=Aborted:K13:1)  //-1
			$ProcState_ptr->{$CurrentProcess_i}:="Aborted"
			LISTBOX SET ROW FONT STYLE:C1268(*; "PV_ProcList_ab"; $CurrentProcess_i; Italic:K14:3)
			LISTBOX SET ROW COLOR:C1270(*; "PV_ProcList_ab"; $CurrentProcess_i; 0x6600)
			$icon_ptr->{$CurrentProcess_i}:=Form:C1466.stateIcon_7
			
		: ($procState=Delayed:K13:2)  //1
			$ProcState_ptr->{$CurrentProcess_i}:="Delayed"
			LISTBOX SET ROW COLOR:C1270(*; "PV_ProcList_ab"; $CurrentProcess_i; 0x0099)
			$icon_ptr->{$CurrentProcess_i}:=Form:C1466.stateIcon_1
			
		: ($procState=Does not exist:K13:3)  //-100
			$ProcState_ptr->{$CurrentProcess_i}:="Does not exist"
			$icon_ptr->{$CurrentProcess_i}:=Form:C1466.stateIcon_7
			
		: ($procState=Executing:K13:4)  //0
			$ProcState_ptr->{$CurrentProcess_i}:="Executing"
			LISTBOX SET ROW FONT STYLE:C1268(*; "PV_ProcList_ab"; $CurrentProcess_i; Underline:K14:4)
			$icon_ptr->{$CurrentProcess_i}:=Form:C1466.stateIcon_0
			
			
		: ($procState=6)  // _o_ Hidden modal dialog)  //6
			$ProcState_ptr->{$CurrentProcess_i}:="Hidden modal dialog"
			LISTBOX SET ROW COLOR:C1270(*; "PV_ProcList_ab"; $CurrentProcess_i; 0x00FF0000)
			$icon_ptr->{$CurrentProcess_i}:=Form:C1466.stateIcon_6
			
			
		: ($procState=Paused:K13:6)  //5
			$ProcState_ptr->{$CurrentProcess_i}:="Paused"
			LISTBOX SET ROW COLOR:C1270(*; "PV_ProcList_ab"; $CurrentProcess_i; 0x00333333)
			$icon_ptr->{$CurrentProcess_i}:=Form:C1466.stateIcon_5
			
		: ($procState=Waiting for input output:K13:7)  //3
			$ProcState_ptr->{$CurrentProcess_i}:="Waiting for input output"
			LISTBOX SET ROW COLOR:C1270(*; "PV_ProcList_ab"; $CurrentProcess_i; 0x00FF3333)
			$icon_ptr->{$CurrentProcess_i}:=Form:C1466.stateIcon_3
			
			
		: ($procState=Waiting for internal flag:K13:8)  //4
			$ProcState_ptr->{$CurrentProcess_i}:="Waiting for internal flag"
			LISTBOX SET ROW COLOR:C1270(*; "PV_ProcList_ab"; $CurrentProcess_i; 0x00FF2389)
			$icon_ptr->{$CurrentProcess_i}:=Form:C1466.stateIcon_4
			
			
		: ($procState=Waiting for user event:K13:9)  //2
			$ProcState_ptr->{$CurrentProcess_i}:="Waiting for user event"
			LISTBOX SET ROW COLOR:C1270(*; "PV_ProcList_ab"; $CurrentProcess_i; 0x00FF6600)
			$icon_ptr->{$CurrentProcess_i}:=Form:C1466.stateIcon_2
			
	End case 
	
	
	$ProcTime_ptr->{$CurrentProcess_i}:=$procTime\60
	
	
End for 

REDRAW WINDOW:C456
